import { AsyncStorage } from "react-native";

// Services
import BlogsService from "../services/BlogsService";
// Constants
import { BASE_URL } from "../api/ApiConstans";
import CONSTANTS from "../utils/Constants";

export default BlogsViewModel = {
    getBlogPosts,
    getDocumentsBaseUrl,
    goToBlogPostScreen
}

/**
 * Get blogs entries of the default site
 * @returns {Promise<Any>}
 */
async function getBlogPosts() {
    const siteId = await AsyncStorage.getItem(CONSTANTS.DEFAULT_SITE_KEY);
    return BlogsService.getBlogPosts(siteId);
}

/**
 * Get base url for documents in Liferay
 * @returns {String} base URL for documents in Liferay
 */
function getDocumentsBaseUrl() {
    return BASE_URL.replace("/o/", "");
}

/**
 * Go to Blog screen to show the information of the blogpost provided
 * @param {Object} blogPost
 * @param {Navigation} navigation
 */
function goToBlogPostScreen(blogPost, navigation) {
    navigation.navigate('Blog', blogPost);
}
