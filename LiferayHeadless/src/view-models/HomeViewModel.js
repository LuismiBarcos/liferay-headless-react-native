import { AsyncStorage } from "react-native";

export default HomeViewModel = {
    logout
}

/**
 * Sign out of the application deleting the saved credentials
 * @param {Navigation} navigation contains the navigation through the app
 */
async function logout(navigation) {
    await AsyncStorage.clear();
    navigation.navigate('Auth');
}