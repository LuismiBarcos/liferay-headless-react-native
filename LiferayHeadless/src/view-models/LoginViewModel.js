// Services
import LoginService from '../services/LoginService';

export default LoginViewModel = {
    doLogin,
    saveCredentials
}

/**
 * Do the login in the portal
 * @param {String} username
 * @param {String} password
 * @returns {Promise} True or false depending on the result of the login
 */
function doLogin(username, password) {
    return LoginService.checkLogin(username, password);
}

/**
 * Use Login service to save the credentials
 * @param {String} username
 * @param {String} password
 * @returns {Promise<Boolean>} True if save the credentials properly
 */
function saveCredentials(username, password) {
    return LoginService.saveCredentials(username, password);
}