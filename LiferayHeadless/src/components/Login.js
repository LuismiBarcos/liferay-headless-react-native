import React from 'react';
import { StyleSheet, View, Text, Button, Alert } from 'react-native';
import { TextInput } from 'react-native-gesture-handler';

// ViewModels
import LoginViewModel from '../view-models/LoginViewModel';

/**
 * Login Screen of the application. When the user log in correctly, the
 * credencials will be save
 */
export default Login = ({navigation}) => (<LoginView navigation={navigation} />)
// Specified Login Screen's navigation bar title
Login.navigationOptions = () => {
    return {
        title: 'Log in'
    }
}

// Functional component with view logic
function LoginView({navigation}) {
    // Attributes
    const [username, onChangeUsername] = React.useState('');
    const [password, onChangePassword] = React.useState('');

    // Functions
    function doLogin() {
        LoginViewModel.doLogin(username, password)
        .then(async result => {
            if(result === true) {
                await LoginViewModel.saveCredentials(username, password)
                navigation.navigate('App');
            } else {
                showSimpleAlert('Login fails', 'Incorrect username or password');
            }
        });
    }

    function showSimpleAlert(title, message) {
        Alert.alert(
            title,
            message,
            [
              {text: 'OK', onPress: () => console.log('OK Pressed')},
            ],
            {cancelable: false},
          );
    }

    // Main render
    return(
        <View style={styles.container}>
            <GenericTextInput
                labelText="Enter yout username"
                value={username}
                callback={onChangeUsername}
                isSecureTextEntry={false}
            />
            <GenericTextInput
                labelText="Enter your password"
                value={password}
                callback={onChangePassword}
                isSecureTextEntry={true}
            />
            <Button title="Log in" onPress={doLogin}></Button>
        </View>
    );

    // Renders
    function GenericTextInput({labelText, value, callback, isSecureTextEntry}) {
        return(
            <View>
                <Text>{labelText}</Text>
                <TextInput
                    onChangeText={text => callback(text)}
                    value={value}
                    style={styles.textInput}
                    secureTextEntry={isSecureTextEntry}
                />
            </View>
        );
    }
}

// Styles
const styles = StyleSheet.create({
    container: {
        flex : 1,
        alignItems: "center",
        justifyContent: "center"
    },
    textInput: {
        width: 200,
        height: 20,
        borderWidth: 1,
        margin: 10
    }
})