import React from 'react';
import { View, Text, StyleSheet, Image, Dimensions } from "react-native"
import { ScrollView } from 'react-native-gesture-handler';

// ViewModels
import BlogsViewModel from "../view-models/BlogsViewModel";

/**
 * Blog Screen. This screen show the image, title and article body of a blogpost
 */
export default Blog = ({navigation}) => (<BlogRender state={navigation.state} />)

// Functional components with view logic
function BlogRender({state}) {
    const blog = state.params.item;
    const URL = BlogsViewModel.getDocumentsBaseUrl() + blog.image.contentUrl;
    const articleBodyWithoutHtml = blog.articleBody.replace(/<\/?[^>]+(>|$)/g, "");
    return(
        <ScrollView contentContainerStyle={styles.container}>
            <Image
                style={styles.image}
                source={{ uri: URL }}
            />
            <Text style={styles.title}>
                {blog.headline}
            </Text>
            <Text style={styles.articleBody}>
                {articleBodyWithoutHtml}
            </Text>
        </ScrollView>
    );
}

// Styles
const styles = StyleSheet.create({
    container: {
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        marginHorizontal: 10,
    },
    image: {
        width: 500,
        height: 150,
        resizeMode: "contain",
        marginBottom: 8,
    },
    title: {
        fontSize: 24,
        fontWeight: "600"
    },
    articleBody: {
        marginBottom: 16
    }
});