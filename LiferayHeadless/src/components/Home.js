import React, { useEffect } from 'react';
import { StyleSheet, View, Text, Button, AsyncStorage, Image } from 'react-native';
import { FlatList, TouchableHighlight } from 'react-native-gesture-handler';

// ViewModels
import BlogsViewModel from '../view-models/BlogsViewModel';
import HomeViewModel from '../view-models/HomeViewModel';

/**
 * Home screen of the application. This screen is shown when the user logs in
 * correctly
 * @param {Navigation} navigation contains the navigation through the app
 */
export default Home = ({navigation}) => ( <BlogsEntries navigation={navigation} /> )

// Specified Home Screen's navigation bar title and right button
Home.navigationOptions = ({navigation}) => {
    return {
        title: 'Home',
        headerRight: () => <Button title="Sign out" onPress={() => HomeViewModel.logout(navigation)}/>
    }
}

// Functional components with view logic
function BlogsEntries({navigation}) {
    const [blogPosts, setBlogPosts] = React.useState({});

    useEffect(() => {
        BlogsViewModel.getBlogPosts().then(setBlogPosts);
    }, []);

    // Main render
    return(
        <View style={{marginBottom: 100}}>
            <FlatList
                data={blogPosts.items}
                renderItem={( blogPost ) => <Card blogPost={blogPost} navigation={navigation}/>}
                keyExtractor={blogPost => blogPost.id.toString()}
                contentContainerStyle={styles.ListStyle}
            />
        </View>
    );

    // Renders
    function Card({blogPost, navigation}) {
        const URL = BlogsViewModel.getDocumentsBaseUrl() + blogPost.item.image.contentUrl;
        return(
            <TouchableHighlight onPress={() => BlogsViewModel.goToBlogPostScreen(blogPost, navigation)}>
            <View style={styles.container} >
                <Image
                style={styles.image}
                source={{ uri: URL }}
                />
                <View style={styles.titleBackground}>
                    <Text style={styles.title}>{blogPost.item.headline}</Text>
                </View>
            </View>
            </TouchableHighlight>
        );
    }
}

// Styles
const styles = StyleSheet.create({
    ListStyle: {
        display: "flex",
        alignItems: "center"
    },
    container: {
        width: 380,
        height: 380,
        flexDirection: "row",
        margin: 2
    },
    image: {
        width: 380,
        height: 380,
        position: 'absolute'
    },
    titleBackground: {
        flex: 1,
        backgroundColor: 'rgba(0, 0, 0, 0.5)',
        alignSelf: 'flex-end'
    },
    title: {
        color: 'white',
        fontSize: 20,
        margin: 6
    }
});