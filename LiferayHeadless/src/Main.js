import React from 'react';
import { ActivityIndicator, AsyncStorage, StatusBar, StyleSheet, View } from 'react-native';
import { createSwitchNavigator, createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack'

// App Screens
import Login from './components/Login';
import Home from './components/Home';
import Blog from './components/Blog'

/**
 * Main functional component which load the content of the app
 */
export default Main = () => (<AppContainer />)

/**
 * Aux functional component to show a loading screen while check if exists a username
 * and password
 */
const AuthLoadingScreen = ({navigation}) => {
  bootstrapAsync()

  async function bootstrapAsync() {
    const username = await AsyncStorage.getItem('USERNAME');
    const password = await AsyncStorage.getItem('PASSWORD');
    navigation.navigate(username && password ? 'App' : 'Auth');
  }

  return <View style={styles.container}>
           <ActivityIndicator />
           <StatusBar barStyle="default" />
         </View>
};

// Declaration of the stack navigation
const AppStack = createStackNavigator({ Home: Home, Blog: Blog });
const AuthStack = createStackNavigator({ SignIn: Login });
const AppContainer = createAppContainer(createSwitchNavigator(
  {
    AuthLoading: AuthLoadingScreen,
    App: AppStack,
    Auth: AuthStack,
  },
  {
    initialRouteName: 'AuthLoading',
  })
);

// Styles
const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
});