import { AsyncStorage } from "react-native";

// Api services
import Connections from "../api/Connections";
import Authentication from "../api/Authentication";
import  * as API_CONSTANTS from "../api/ApiConstans";

// Utils
import CONSTANTS from "../utils/Constants";

export default LoginService = {
    checkLogin,
    saveCredentials
}

/**
 * Checks if the login credentials allow connection with Liferay headless APIs
 * @param {String} username
 * @param {String} password
 * @returns {Promise<Boolean>} True if the login credentials are correct or false in other case
 */
async function checkLogin(username, password) {
    const basicAuthToken = Authentication.createBasicAuthorizationToken(username, password);
    const headers = Connections.createHeadersBasicAuthorization(basicAuthToken);
    try {
        return await Connections.doApiCall('GET', headers, API_CONSTANTS.BASE_URL, API_CONSTANTS.LOGIN.LOGIN_CHECK)
        .then(response => response.status === 200);
    } catch (error) {
        console.log("Login check fails ", error);
    }
}

/**
 * Save credentials in case of the username and password are valir to call headless APIs
 * @param {String} username
 * @param {String} password
 * @returns {Promise<Boolean>} True if save the credentials properly
 */
async function saveCredentials(username, password) {
    await AsyncStorage.setItem(CONSTANTS.USERNAME_KEY, username);
    await AsyncStorage.setItem(CONSTANTS.PASSWORD_KEY, password);
    await saveSiteId(username, password);
}

/**
 * Save the default site Id based on the default site name
 * @param {String} username
 * @param {String} password
 */
async function saveSiteId(username, password) {
    const basicAuthToken = Authentication.createBasicAuthorizationToken(username, password);
    try {
        await Connections.getSiteId(CONSTANTS.DEFAULT_SITE_KEY, basicAuthToken)
        .then(async siteId => await AsyncStorage.setItem(CONSTANTS.DEFAULT_SITE_KEY, siteId.toString()));
    } catch (error) {
        console.log("Error saving siteId ", error)
    }
}