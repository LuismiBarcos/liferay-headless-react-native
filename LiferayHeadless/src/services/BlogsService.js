// Api services
import Connections from "../api/Connections";
import Authentication from "../api/Authentication";
import { METHODS, BLOGS } from "../api/ApiConstans";
// Util constants
import CONSTANTS from "../utils/Constants";

import { AsyncStorage } from "react-native";

export default BlogsService = {
    getBlogPosts
}

/**
 * Get the blog posts of a concrete site
 * @param {String} siteId Id of the site where are the blogs
 * @param {Number} page Page number to recover
 * @param {Number} pageSize Number of items to recover
 * @returns {Promise<Any>} Returns a promise with the json of the blogs posts
 */
async function getBlogPosts(siteId, page = 1, pageSize = 20) {
    const username = await AsyncStorage.getItem(CONSTANTS.USERNAME_KEY);
    const password = await AsyncStorage.getItem(CONSTANTS.PASSWORD_KEY);
    const basicAuthToken = Authentication.createBasicAuthorizationToken(username, password);
    const headers = Connections.createHeadersBasicAuthorization(basicAuthToken);
    const response = await Connections.doApiCall(METHODS.GET, headers, BLOGS.BASE_URL, siteId, BLOGS.BLOG_POSTINGS);
    return response.json();
}