import { BASE_URL, SITES } from "./ApiConstans";

export default Connections = {
    createHeadersBasicAuthorization,
    doApiCall,
    getSiteId
}

/**
 * Returns the site id of omnichanel, which is created with the name NewLiferay
 * @param {String} siteKey Key of the site to search
 * @returns {Promise<Number>} Site id of omnichanel
 */
async function getSiteId(siteKey, basicAuthToken) {
    const headers = createHeadersBasicAuthorization(basicAuthToken);
    const response = await doApiCall('GET', headers, BASE_URL, SITES.GET_SITES);
    const json = await response.json();
    const siteIdArray = json.items.filter(item => item.key === siteKey).map(item => item.id);
    return siteIdArray[0] ? siteIdArray[0] : 0;
}

/**
 * Do the API call to a especif url with specific headers and method
 * @param {String} method Method of the request (GET, POST, PUT...)
 * @param {Headers} headers Headers of the request
 * @param  {...any} urlStrings Url string in order to build the url
 * @returns {Promise<Response>} Response of the API call
 */
async function doApiCall(method, headers, ...urlStrings) {
    const url = createURL(urlStrings);
    try {
        return await fetch(url, {
            method: method,
            headers: headers
        });
    }
    catch (err) {
        console.log("Request failed ", err);
    }
}

/**
 * Create URL depending on the strings passed. The URL strings must be in order because the
 * function only concatenate the strings passed.
 * @param  {...any} urlStrings All strings which component the url to do the API call
 * @returns {String} URL compose by the string passed.
 */
function createURL(urlStrings) {
    return urlStrings.reduce((accumulator, current) => accumulator + current);
}

/**
 * Create a header with the basic token authentication and a content-type as application/json
 * @param {String} basicAuthToken Token in base64 to use in Basic authentication
 * @returns {Headers} Headers for the request
 */
function createHeadersBasicAuthorization(basicAuthToken) {
    const headers = new Headers();
    headers.set('Authorization', 'Basic ' + basicAuthToken);
    headers.set('Content-Type', 'application/json');
    return headers;
}