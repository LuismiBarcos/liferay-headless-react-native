// Private constants
const ADMIN_USER = "headless-admin-user/v1.0/";
const DELIVERY = "headless-delivery/v1.0/";

// Public constants
export const BASE_URL = "https://liferay-omnichannel-demo-72.liferay.org.es/o/";
export const LOGIN = { LOGIN_CHECK: ADMIN_USER + "user-accounts" }
export const SITES = { GET_SITES: ADMIN_USER + "my-user-account/sites" }
export const METHODS = {
    GET: 'GET',
    POST: 'POST'
}
export const BLOGS = {
    BASE_URL: BASE_URL + DELIVERY + "sites/",
    BLOG_POSTINGS: "/blog-postings"
}

